﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Xunit;
using Zadatak3.Data;
using Zadatak3.Repository.Implementations;
using System;
using Zadatak3.Models.Enum;
using Zadatak3.Data.DbModels;

namespace Zadatak3.Tests.Repository
{
    public class ToDoRepositoryShould
    {
        [Fact]
        public async Task AddAndRemoveItem()
        {
            var item1 = new DbToDoItem
            {
                Id = new Guid(),
                Deadline = new DateTime(2008, 4, 1),
                Start = new DateTime(2008, 1, 1),
                Description = "test",
                UserId = null,
                Status = ToDoStatus.Done,
                Name = "prvi"
            };

            var item2 = new DbToDoItem
            {
                Id = new Guid("d5faab1f-3fac-4a71-bff1-2711322d703b"),
            };

            using (var context = new UserDbContext(DbContextOptions()))
            {
                var service = new ToDoRepository(context);
                await service.AddAsync(item1);
                await service.AddAsync(item2);
                await service.RemoveAsync(new Guid("d5faab1f-3fac-4a71-bff1-2711322d703b"));
                Assert.Equal(1, await context.ToDoItems.CountAsync());
                Assert.Equal("prvi", item1.Name);
            }
        }

        [Fact]
        public async Task AddItem()
        {
            var item = new DbToDoItem
            {
                Id = new Guid(),
                Deadline = new DateTime(2008, 4, 1),
                Start = new DateTime(2008, 1, 1),
                Description = "test",
                UserId = null,
                Status = ToDoStatus.Done,
                Name = "prvi"
            };

            using (var context = new UserDbContext(DbContextOptions()))
            {
                var service = new ToDoRepository(context);
                await service.AddAsync(item);
                Assert.Equal(1, await context.ToDoItems.CountAsync());
            }
        }

        [Fact]
        public async Task GetAllItems()
        {
            var item1 = new DbToDoItem { Id = new Guid() };
            var item2 = new DbToDoItem { Id = new Guid() };
            var item3 = new DbToDoItem { Id = new Guid() };

            using (var context = new UserDbContext(DbContextOptions()))
            {
                var service = new ToDoRepository(context);
                await service.AddAsync(item1);
                await service.AddAsync(item2);
                await service.AddAsync(item3);
                var items = await service.GetAllAsync();
                Assert.Equal(3, items.Count);
            }
        }

        [Fact]
        public async Task UpdateItem()
        {
            var item = new DbToDoItem
            {
                Id = new Guid("d5faab1f-3fac-4a71-bff1-2711322d703b"),
                Deadline = new DateTime(2008, 4, 1),
                Start = new DateTime(2008, 1, 1),
                Description = "test",
                UserId = null,
                Status = ToDoStatus.Done,
                Name = "prvi"
            };

            using (var context = new UserDbContext(DbContextOptions()))
            {
                var service = new ToDoRepository(context);

                await service.AddAsync(item);

                var editItem = await service.FindAsync(new Guid("d5faab1f-3fac-4a71-bff1-2711322d703b"));

                editItem.Name = "drugi";

                await service.UpdateAsync(editItem);

                Assert.Equal("drugi", item.Name);
            }
        }

        private DbContextOptions DbContextOptions()
        {
            var options = new DbContextOptionsBuilder<UserDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            return options;
        }
    }
}