﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Zadatak3.Repository.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Zadatak3.Data.DbModels;
using System.Security.Claims;
using System.Collections.Generic;
using Zadatak3.Data;
using Microsoft.EntityFrameworkCore;
using System;

namespace Zadatak3.Repository.Implementations
{
    public class UserRepository : IUserRepository
    {
        private UserDbContext _context;
        private RoleManager<IdentityRole<Guid>> _roleManager;
        private SignInManager<DbUser> _signinManager;
        private UserManager<DbUser> _userManager;

        public UserRepository(UserManager<DbUser> userManager, SignInManager<DbUser> signInManager,
                                        RoleManager<IdentityRole<Guid>> roleManager, UserDbContext context)
        {
            _userManager = userManager;
            _signinManager = signInManager;
            _roleManager = roleManager;
            _context = context;
        }

        public async Task<IdentityResult> AddToRoleAsync(DbUser user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> CreateRoleAsync(IdentityRole<Guid> role)
        {
            return await _roleManager.CreateAsync(role);
        }

        public async Task<DbUser> FindByNameAsync(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        public async Task<DbUser> GetUserIdAsync(ClaimsPrincipal principal)
        {
            return await _userManager.GetUserAsync(principal);
        }

        public async Task<List<DbUser>> GetUserProfileAsync()
        {
            return await _context.Users.Include(usr => usr.UserProfile).ToListAsync();
        }

        public async Task<bool> IsinRoleAsync(DbUser user, string role)
        {
            return await _userManager.IsInRoleAsync(user, role);
        }

        public bool isSignedIn(ClaimsPrincipal principal)
        {
            return _signinManager.IsSignedIn(principal);
        }

        public async Task LogOffAsync()
        {
            await _signinManager.SignOutAsync();
        }

        public async Task<SignInResult> PasswordLoginAsync(string username, string passwordHash, bool isPersistent, bool lockout)
        {
            return await _signinManager.PasswordSignInAsync(username, passwordHash, isPersistent, lockout);
        }

        public async Task<IdentityResult> RegisterAsync(DbUser user, string password)
        {
            return await _userManager.CreateAsync(user, password);
        }
    }
}