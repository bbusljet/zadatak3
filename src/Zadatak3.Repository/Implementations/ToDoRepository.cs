﻿using System.Collections.Generic;
using Zadatak3.Data;
using Zadatak3.Repository.Interfaces;
using Zadatak3.Data.DbModels;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;

namespace Zadatak3.Repository.Implementations
{
    public class ToDoRepository : IToDoRepository
    {
        private UserDbContext _context;

        public ToDoRepository(UserDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(DbToDoItem item)
        {
            _context.ToDoItems.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task<DbToDoItem> FindAsync(Guid id)
        {
            return await _context.ToDoItems.SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task<List<DbToDoItem>> GetAllAsync()
        {
            return await _context.ToDoItems.ToListAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var item = _context.ToDoItems.First(i => i.Id == id);
            _context.ToDoItems.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(DbToDoItem item)
        {
            _context.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}