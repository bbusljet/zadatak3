﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Zadatak3.Data.DbModels;

namespace Zadatak3.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<IdentityResult> AddToRoleAsync(DbUser user, string role);

        Task<IdentityResult> CreateRoleAsync(IdentityRole<Guid> role);

        Task<DbUser> FindByNameAsync(string userName);

        Task<DbUser> GetUserIdAsync(ClaimsPrincipal principal);

        Task<List<DbUser>> GetUserProfileAsync();

        Task<bool> IsinRoleAsync(DbUser user, string role);

        bool isSignedIn(ClaimsPrincipal principal);

        Task LogOffAsync();

        Task<SignInResult> PasswordLoginAsync(string username, string passwordHash, bool isPersistent, bool lockout);

        Task<IdentityResult> RegisterAsync(DbUser user, string password);
    }
}