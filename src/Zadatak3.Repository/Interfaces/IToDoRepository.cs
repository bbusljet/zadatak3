﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak3.Data.DbModels;

namespace Zadatak3.Repository.Interfaces
{
    public interface IToDoRepository
    {
        Task AddAsync(DbToDoItem item);

        Task<DbToDoItem> FindAsync(Guid id);

        Task<List<DbToDoItem>> GetAllAsync();

        Task RemoveAsync(Guid id);

        Task UpdateAsync(DbToDoItem item);
    }
}