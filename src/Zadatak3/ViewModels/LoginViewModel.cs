﻿using System.ComponentModel.DataAnnotations;

namespace Zadatak3.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter your password"), DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter your User Name")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
    }
}