﻿$(function () {
    jQuery.validator.addMethod("tododeadlinedate", function (value, element) {
        let startDate = $(".start").val();

        return Date.parse(value) > Date.parse(startDate);
    });

    jQuery.validator.unobtrusive.adapters.add("tododeadlinedate", function (options) {
        options.rules["tododeadlinedate"] = {};
        options.messages["tododeadlinedate"] = options.message;
    });

}(jQuery));