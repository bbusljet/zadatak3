﻿$(function () {
    jQuery.validator.addMethod("todostartdate", function (value, element, params) {
        let today = new Date();
        
        return Date.parse(value) > Date.parse(today);
    });

    jQuery.validator.unobtrusive.adapters.add("todostartdate", function (options) {
        options.rules["todostartdate"] = {};
        options.messages["todostartdate"] = options.message;
    });
}(jQuery));