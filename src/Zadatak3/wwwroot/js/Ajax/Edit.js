﻿$(document).ready(function () {
    $("#js-editForm").on("submit", function (e) {
        e.preventDefault();
        let editData = $(this).serialize();
        let button = $(e.target);
        
        bootbox.dialog({
            title: 'Confirm',
            message: "<p>Are you sure you want to edit the item ?</p>",
            buttons: {
                ok: {
                    label: "Ok",
                    className: 'btn-success',
                    callback: function () {
                        $.ajax({
                            url: "/Admin/Edit/" + $(button).attr("data-item-id"),
                            method: "POST",
                            data: editData,
                            dataType: "text"
                        })
                            .done(function () {
                                bootbox.alert("Item has been successfully edited.");
                            })
                            .fail(function () {
                                bootbox.alert("Something went wrong");
                            });
                    }
                },

                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    });
});