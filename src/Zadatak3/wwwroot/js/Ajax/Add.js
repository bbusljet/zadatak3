﻿$(document).ready(function () {
    $("#js-addForm").on("submit", function (e) {
        e.preventDefault();
        let formData = $(this).serialize();

        bootbox.dialog({
            title: 'Confirm',
            message: "<p>Are you sure you want to add the item ?</p>",
            buttons: {
                ok: {
                    label: "Ok",
                    className: 'btn-success',
                    callback: function () {
                        $.ajax({
                            url: "/Admin/Add/",
                            method: "POST",
                            data: formData,
                            dataType: "html"
                        })
                            .done(function () {
                                bootbox.alert("Item has been successfully added.");
                            })
                            .fail(function () {
                                bootbox.alert("Something went wrong");
                            });
                    }
                },

                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function () {
                        bootbox.hideAll();
                    }
                }
            }
        });
    });
});