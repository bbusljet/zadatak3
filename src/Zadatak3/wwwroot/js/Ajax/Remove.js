﻿$(document).ready(function () {
    $("#js-todos .js-Remove").on("click", function (e) {
        let button = $(e.target);
        bootbox.dialog({
            title: 'Confirm',
            message: "<p>Are you sure you want to remove this item ?</p>",
            buttons: {
                no: {
                    label: "No",
                    className: 'btn-default',
                    callback: function () {
                        bootbox.hideAll();
                    }
                },

                yes: {
                    label: "Yes",
                    className: 'btn-danger',
                    callback: function () {
                        $.ajax({
                            url: "/Admin/Remove/" + $(button).attr("data-item-id"),
                            method: "POST",
                        })
                            .done(function () {
                                $(button).parents("tr").fadeOut(function () {
                                    $(this).remove();
                                    bootbox.alert("Item has been successfully removed");
                                });
                            })
                            .fail(function () {
                                bootbox.alert("Something went wrong");
                            });
                    }
                }
            }
        });
    });
});