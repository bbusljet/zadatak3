﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Zadatak3.Areas.Admin.ViewModels;
using Zadatak3.GuidExtensions;
using Zadatak3.Models.Models;
using Zadatak3.Services.Interfaces;

namespace Zadatak3.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private IToDoService _toDoService;
        private IUserService _userService;

        public AdminController(IToDoService toDoService, IUserService userService)
        {
            _toDoService = toDoService;
            _userService = userService;
        }

        [HttpGet]
        [Route("Admin/Add")]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [Route("Admin/Add")]
        public async Task<IActionResult> Add([FromForm]AddItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = await _userService.GetUserIdAsync(HttpContext.User);
                var item = new ToDoItem
                {
                    Id = Guid.NewGuid().GenerateSequentialGuid(),
                    UserId = userId,
                    Start = model.Start,
                    Deadline = model.Deadline,
                    Description = model.Description,
                    Name = model.Name,
                    Status = model.Status
                };

                await _toDoService.AddAsync(item);

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        [Route("Admin/Edit/{id}")]
        public async Task<ActionResult> Edit(Guid Id)
        {
            var item = await _toDoService.FindAsync(Id);
            if (item == null)
            {
                return NotFound();
            }
            var vmItem = Mapper.Map<EditItemViewModel>(item);
            
            return View(vmItem);
        }

        [HttpPost]
        [Route("Admin/Edit/{id}")]
        public async Task<ActionResult> Edit([FromForm] EditItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                var item = new ToDoItem
                {
                    Id = model.TodoId,
                    Start = model.Start,
                    Deadline = model.Deadline,
                    Name = model.Name,
                    Description = model.Description,
                    Status = model.Status
                };
                await _toDoService.UpdateAsync(item);
                return RedirectToAction("Index", "Admin");
            }

            return View(model);
        }

        [HttpGet]
        [Route("Admin/Index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("Admin/List")]
        public async Task<IActionResult> Items()
        {
            var items = await _toDoService.GetAllAsync();

            if (items.Count == 0)
            {
                return View("NoItems");
            }

            return View("Items", items);
        }

        [HttpPost]
        [Route("Admin/Logout")]
        public async Task<IActionResult> Logout()
        {
            await _userService.LogOffAsync();

            return RedirectToAction("Login", "User", new { area = "" });
        }

        [HttpPost]
        [Route("Admin/Remove/{id}")]
        public async Task<IActionResult> Remove(Guid id)
        {
            var item = await _toDoService.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            await _toDoService.RemoveAsync(id);

            return RedirectToAction("items", "Admin");
            
        }
    }
}