﻿using System;
using System.ComponentModel.DataAnnotations;
using Zadatak3.Models.Enum;
using Zadatak3.Validation;

namespace Zadatak3.Areas.Admin.ViewModels
{
    public class EditItemViewModel
    {
        [DataType(DataType.Date)]
        [ToDoDeadlineDate("Start", ErrorMessage ="Item can't end before start")]
        [Required(ErrorMessage = "Please enter a date"), Display(Name = "Deadline")]
        public DateTime Deadline { get; set; }

        [Required(ErrorMessage = "Please enter a description"), StringLength(250), Display(Name = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter a name"), StringLength(100), MinLength(5, ErrorMessage = "Minimum Name length is 5"), Display(Name = "Name")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [ToDoStartDate(ErrorMessage ="item can't be created in the past")]
        [Required(ErrorMessage = "Please enter a date"), Display(Name = "Start")]
        public DateTime Start { get; set; }

        [Required(ErrorMessage = "Please set a status"), Display(Name = "Status")]
        public ToDoStatus Status { get; set; }

        public Guid TodoId { get; set; }
    }
}