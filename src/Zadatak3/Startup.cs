﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Zadatak3.Data;
using Zadatak3.Data.DbModels;
using Zadatak3.MapperConfiguration;
using Zadatak3.Models.Settings;
using Zadatak3.Repository.Implementations;
using Zadatak3.Repository.Interfaces;
using Zadatak3.Services.Implementations;
using Zadatak3.Services.Interfaces;

namespace Zadatak3
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                //builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceScopeFactory scopeFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Admin}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=ToDo}/{action=Index}/{id?}");
            });

            scopeFactory.SeedInitialData();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            var connectionString = Configuration["AppSettings:DbContextSettings:ConnectionString"];
            services.AddDbContext<UserDbContext>(options =>
                options.UseNpgsql(connectionString));

            services.AddIdentity<DbUser, IdentityRole<Guid>>(x =>
            {
                x.Password.RequiredLength = 6;
                x.Password.RequireUppercase = true;
                x.Password.RequireLowercase = true;
                x.Password.RequireNonAlphanumeric = false;
            })
                .AddEntityFrameworkStores<UserDbContext, Guid>()
                .AddDefaultTokenProviders();

            AutoMapperConfigurations.Configure();
            services.AddMvc();

            // Add application services.
            services.AddTransient<IToDoService, ToDoService>();
            services.AddTransient<IUserService, UserService>();
            services.AddScoped<IToDoRepository, ToDoRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.Configure<ApplicationSettings>(Configuration.GetSection("AppSettings"));
        }
    }
}