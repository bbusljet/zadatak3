﻿using AutoMapper;
using Zadatak3.Areas.Admin.ViewModels;
using Zadatak3.Data.DbModels;
using Zadatak3.Models.Models;
using Zadatak3.ViewModels;

namespace Zadatak3.MapperConfiguration
{
    public class AutoMapperConfigurations
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfiles(new[] { "Zadatak3.Repository" });
                cfg.CreateMap<User, DbUser>()
                    .ForMember(m => m.UserName, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(m => m.PasswordHash, opt => opt.MapFrom(src => src.PasswordHash))
                    .ForMember(m => m.Email, opt => opt.MapFrom(src => src.Email))
                    .ForMember(m => m.Id, opt => opt.MapFrom(src => src.Id));

                cfg.CreateMap<DbUser, User>();
                cfg.CreateMap<UserProfile, DbUserProfile>();
                cfg.CreateMap<DbUserProfile, UserProfile>();

                cfg.CreateMap<DbToDoItem, ToDoItem>().MaxDepth(3).ReverseMap();

                cfg.CreateMap<EditItemViewModel, ToDoItem>().ReverseMap().ForMember(vm => vm.TodoId, opt => opt.MapFrom(item => item.Id));
                
                cfg.CreateMap<RegisterViewModel, User>()
                .ForMember(vm => vm.UserProfile, opt => opt.MapFrom(src => new UserProfile
                {
                    FirstName = src.FirstName,
                    LastName = src.FirstName,
                    City = src.City,
                    Adress = src.Adress,
                    PostalCode = src.PostalCode,
                    Country = src.Country
                }));
            });
        }
    }
}