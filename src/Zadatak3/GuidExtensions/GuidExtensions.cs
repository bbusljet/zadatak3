﻿using System;

namespace Zadatak3.GuidExtensions
{
    public static class GuidExtensions
    {
        public static Guid GenerateSequentialGuid(this Guid source)
        {
            var guid = source.ToByteArray();

            var time = GetTicks();

            var timeBytes = BitConverter.GetBytes(time);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(timeBytes);

            var guidBytes = new byte[16];

            Buffer.BlockCopy(timeBytes, 2, guidBytes, 0, 6);
            Buffer.BlockCopy(guid, 0, guidBytes, 6, 10);

            return new Guid(guidBytes);
        }

        private static long GetTicks()
        {
            return DateTime.UtcNow.Ticks / 10000;
        }
    }
}