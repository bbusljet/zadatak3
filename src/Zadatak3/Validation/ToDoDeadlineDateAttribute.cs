﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Zadatak3.Validation
{
    public class ToDoDeadlineDateAttribute : ValidationAttribute, IClientModelValidator
    {
        private readonly string _comparisonProperty;

        public ToDoDeadlineDateAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-tododeadlinedate", ErrorMessage);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);
            
            // this works with different viewmodels with the same properties (AddToDoViewModel,AddItemViewModel)
            object instance = validationContext.ObjectInstance;
            Type type = instance.GetType(); // get the type information of the object instance
            PropertyInfo deadlineProperty = type.GetProperty("Deadline"); //specify the name of the property we want to validate
            object deadlinePropertyValue = deadlineProperty.GetValue(instance); // get the actual value of the Deadline property

            var comparisonValue = (DateTime)property.GetValue(validationContext.ObjectInstance); // get the value of the property from the parameter

            if ((DateTime)deadlinePropertyValue < comparisonValue)
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}