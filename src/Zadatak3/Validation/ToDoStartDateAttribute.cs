﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Zadatak3.Validation
{
    public class ToDoStartDateAttribute : ValidationAttribute, IClientModelValidator
    {
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add("data-val-todostartdate", ErrorMessage);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            object instance = validationContext.ObjectInstance;
            Type type = instance.GetType(); // get the type information of the object instance
            PropertyInfo property = type.GetProperty("Start"); // specify the name of the property
            object propertyValue = property.GetValue(instance); // get the actual value from the Start property

            if ((DateTime)propertyValue < DateTime.Now)
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}