﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak3.GuidExtensions;
using Zadatak3.Models.Models;
using Zadatak3.Services.Interfaces;
using Zadatak3.ViewModels;

namespace Zadatak3.Controllers
{
    [Authorize]
    public class ToDoController : Controller
    {
        private IToDoService _toDoService;
        private IUserService _userService;

        public ToDoController(IToDoService toDoService, IUserService userService)
        {
            _toDoService = toDoService;
            _userService = userService;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ActionResult> Add(AddToDoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = await _userService.GetUserIdAsync(HttpContext.User);

                var toDoItem = new ToDoItem
                {
                    Id = Guid.NewGuid().GenerateSequentialGuid(),
                    UserId = userId,
                    Name = model.Name,
                    Description = model.Description,
                    Status = model.Status,
                    Start = model.Start,
                    Deadline = model.Deadline
                };

                await _toDoService.AddAsync(toDoItem);
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        [Route("Add")]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public async Task<List<ToDoItem>> GetAll()
        {
            return await _toDoService.GetAllAsync();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
    }
}