﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Zadatak3.GuidExtensions;
using Zadatak3.Models.Models;
using Zadatak3.Services.Interfaces;
using Zadatak3.ViewModels;

namespace Zadatak3.Controllers
{
    [AllowAnonymous]
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("Login")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.PasswordLoginAsync(model.UserName, model.Password, false, false);
                if (result.Succeeded)
                {
                    var user = await _userService.FindByNameAsync(model.UserName);
                    bool isAdmin = await _userService.IsinRoleAsync(user, "Administrator");

                    if (isAdmin)
                    {
                        return RedirectToAction("Index", "Admin", new { area = "Admin" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "ToDo");
                    }
                }
            }
            ModelState.AddModelError("", "Invalid login");
            return View(model);
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<ActionResult> Logout()
        {
            await _userService.LogOffAsync();
            return RedirectToAction("Index", "ToDo");
        }

        [HttpGet]
        [Route("Profile")]
        public ActionResult Profile()
        {
            return View();
        }

        [HttpGet]
        [Route("Register")]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [Route("Register")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newUser = new User
                {
                    Id = Guid.NewGuid().GenerateSequentialGuid(),
                    UserName = model.UserName,
                    Email = model.Email,
                    UserProfile = new UserProfile
                    {
                        Adress = model.Adress,
                        City = model.City,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Country = model.Country,
                        PostalCode = model.PostalCode
                    }
                };

                var result = await _userService.RegisterAsync(newUser, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Login", "User");
                }
            }
            return View();
        }
    }
}