﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Zadatak3.Services.Interfaces;

namespace Zadatak3.ViewComponents
{
    public class LoginStatusViewComponent : ViewComponent
    {
        private IUserService _userService;

        public LoginStatusViewComponent(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userService.GetUserIdAsync(HttpContext.User);

                return View("LoggedIn", user);
            }
            else
            {
                return View("NotLogged");
            }
        }
    }
}