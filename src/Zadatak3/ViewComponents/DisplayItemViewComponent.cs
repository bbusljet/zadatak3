﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Zadatak3.Services.Interfaces;

namespace Zadatak3.ViewComponents
{
    public class DisplayItemViewComponent : ViewComponent
    {
        private IToDoService _toDoService;
        private IUserService _useService;

        public DisplayItemViewComponent(IToDoService toDoService, IUserService userService)
        {
            _toDoService = toDoService;
            _useService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (!User.Identity.IsAuthenticated)
                return View("NotLoggedIn");

            var userId = await _useService.GetUserIdAsync(HttpContext.User);

            var items = await _toDoService.GetAllByUserIdAsync(userId);

            //var items = await _toDoService.GetItemsDescending();

            if (items.Count == 0)
                return View("NoItems");

            return View("Items", items);
        }
    }
}