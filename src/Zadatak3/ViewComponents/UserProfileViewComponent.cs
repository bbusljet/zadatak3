﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Zadatak3.Services.Interfaces;

namespace Zadatak3.ViewComponents
{
    public class UserProfileViewComponent : ViewComponent
    {
        private IUserService _userService;

        public UserProfileViewComponent(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = await _userService.GetUserIdAsync(HttpContext.User);

                var model = await _userService.GetCurrentUserProfileAsync(userId);

                return View("UserProfile", model);
            }
            else
            {
                return View("NotLogged");
            }
        }
    }
}