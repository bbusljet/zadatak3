﻿using System;
using Zadatak3.Models.Enum;

namespace Zadatak3.Models.Models
{
    public class ToDoItem
    {
        public DateTime Deadline { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public ToDoStatus Status { get; set; }
        public User UserId { get; set; }
    }
}