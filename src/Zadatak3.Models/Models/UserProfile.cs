﻿using System;

namespace Zadatak3.Models.Models
{
    public class UserProfile
    {
        public string Adress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PostalCode { get; set; }
        public Guid UserId { get; set; }
    }
}