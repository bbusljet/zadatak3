﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Zadatak3.Models.Models
{
    public class User : IdentityUser<Guid>
    {
        public List<ToDoItem> ToDoItems { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}