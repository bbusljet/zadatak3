﻿namespace Zadatak3.Models.Enum
{
    public enum ToDoStatus
    {
        ToDo,
        InProgress,
        Done
    }
}