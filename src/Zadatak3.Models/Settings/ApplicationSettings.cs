﻿namespace Zadatak3.Models.Settings
{
    public class AdminUser
    {
        public string Email { get; set; }
        public string UserName { get; set; }
    }

    public class ApplicationSettings
    {
        public AdminUser AdminUser { get; set; }
    }
}