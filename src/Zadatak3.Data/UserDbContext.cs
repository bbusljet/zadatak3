﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using Zadatak3.Data.DbModels;

namespace Zadatak3.Data
{
    public class UserDbContext : IdentityDbContext<DbUser, IdentityRole<Guid>, Guid>
    {
        public UserDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<DbToDoItem> ToDoItems { get; set; }
        public DbSet<DbUserProfile> UserProfile { get; set; }
    }
}