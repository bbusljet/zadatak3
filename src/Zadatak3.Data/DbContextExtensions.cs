﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using Zadatak3.Data.DbModels;
using Zadatak3.Models.Models;
using Zadatak3.Models.Settings;

namespace Zadatak3.Data
{
    public static class DbContextExtensions
    {
        public static async void SeedInitialData(this IServiceScopeFactory scopeFactory)
        {
            using (var serviceScope = scopeFactory.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<UserDbContext>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<DbUser>>();
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole<Guid>>>();
                var appSettings = serviceScope.ServiceProvider.GetService<IOptions<ApplicationSettings>>();

                var adminSettings = appSettings.Value.AdminUser;
                await AddRole(roleManager);
                await AddAdminUser(userManager, adminSettings);

                await context.SaveChangesAsync();
            }
        }

        private static async Task AddAdminUser(UserManager<DbUser> userManager, AdminUser adminUser)
        {
            var findUser = await userManager.FindByEmailAsync(adminUser.Email);
            if (findUser == null)
            {
                var userId = Guid.NewGuid();
                string password = "Admin-1";

                var user = new DbUser
                {
                    Id = userId,
                    Email = adminUser.Email,
                    UserName = adminUser.UserName,
                };

                var createResult = await userManager.CreateAsync(user, password);
                if (createResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "Administrator");
                }
            }
        }

        private static async Task AddRole(RoleManager<IdentityRole<Guid>> roleManager)
        {
            var adminRole = new IdentityRole<Guid>("Administrator");
            var roleExists = await roleManager.RoleExistsAsync("Administrator");
            if (!roleExists)
            {
                await roleManager.CreateAsync(adminRole);
            }
        }
    }
}