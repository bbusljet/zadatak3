﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Zadatak3.Models.Enum;

namespace Zadatak3.Data.DbModels
{
    [Table("ToDoItems")]
    public class DbToDoItem
    {
        public DateTime Deadline { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [Key]
        public Guid Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public DateTime Start { get; set; }
        public ToDoStatus Status { get; set; }

        [Required]
        public DbUser UserId { get; set; }
    }
}