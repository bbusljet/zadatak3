﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zadatak3.Data.DbModels
{
    [Table("UserProfiles")]
    public class DbUserProfile
    {
        [StringLength(50)]
        public string Adress { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string PostalCode { get; set; }

        [Key, ForeignKey("User")]
        public Guid UserId { get; set; }
    }
}