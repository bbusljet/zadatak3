﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Zadatak3.Data.DbModels
{
    public class DbUser : IdentityUser<Guid>
    {
        public List<DbToDoItem> ToDoItems { get; set; }
        public DbUserProfile UserProfile { get; set; }
    }
}