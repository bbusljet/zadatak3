﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Zadatak3.Models.Models;

namespace Zadatak3.Services.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> AddToRoleAsync(User user, string role);

        Task<IdentityResult> CreateRoleAsync(IdentityRole<Guid> role);

        Task<User> FindByNameAsync(string userName);

        Task<List<User>> GetCurrentUserProfileAsync(User user);

        Task<User> GetUserIdAsync(ClaimsPrincipal principal);

        Task<bool> IsinRoleAsync(User user, string role);

        bool isSignedIn(ClaimsPrincipal principal);

        Task LogOffAsync();

        Task<SignInResult> PasswordLoginAsync(string username, string passwordHash, bool isPersistent, bool lockout);

        Task<IdentityResult> RegisterAsync(User user, string password);
    }
}