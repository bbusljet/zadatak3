﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak3.Models.Models;

namespace Zadatak3.Services.Interfaces
{
    public interface IToDoService
    {
        Task AddAsync(ToDoItem item);

        Task<ToDoItem> FindAsync(Guid id);

        Task<List<ToDoItem>> GetAllAsync();

        Task<List<ToDoItem>> GetAllByUserIdAsync(User user);

        Task<List<ToDoItem>> GetItemsDescendingAsync();

        Task RemoveAsync(Guid id);

        Task UpdateAsync(ToDoItem item);
    }
}