﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak3.Models.Models;
using Zadatak3.Repository.Interfaces;
using Zadatak3.Services.Interfaces;
using AutoMapper;
using Zadatak3.Data.DbModels;
using System.Linq;
using System;

namespace Zadatak3.Services.Implementations
{
    public class ToDoService : IToDoService
    {
        private IToDoRepository _toDoRepository;

        public ToDoService(IToDoRepository toDoRepository)
        {
            _toDoRepository = toDoRepository;
        }

        public async Task AddAsync(ToDoItem item)
        {
            var dbItem = Mapper.Map<DbToDoItem>(item);
            await _toDoRepository.AddAsync(dbItem);
        }

        public async Task<ToDoItem> FindAsync(Guid id)
        {
            var dbItem = await _toDoRepository.FindAsync(id);
            return Mapper.Map<ToDoItem>(dbItem);
        }

        public async Task<List<ToDoItem>> GetAllAsync()
        {
            var items = await _toDoRepository.GetAllAsync();
            return Mapper.Map<List<ToDoItem>>(items);
        }

        public async Task<List<ToDoItem>> GetAllByUserIdAsync(User user)
        {
            var dbUser = Mapper.Map<User, DbUser>(user);

            var items = await _toDoRepository.GetAllAsync();

            var filteredItems = items.FindAll(i => i.UserId != null && i.UserId.Id == user.Id);

            return Mapper.Map<List<ToDoItem>>(filteredItems);
        }

        public async Task<List<ToDoItem>> GetItemsDescendingAsync()
        {
            var items = await _toDoRepository.GetAllAsync();

            var descendingItems = items.OrderByDescending(i => i.Id);

            return Mapper.Map<List<ToDoItem>>(descendingItems);
        }

        public async Task RemoveAsync(Guid id)
        {
            await _toDoRepository.RemoveAsync(id);
        }

        public async Task UpdateAsync(ToDoItem item)
        {
            var dbItem = Mapper.Map<ToDoItem, DbToDoItem>(item);
            await _toDoRepository.UpdateAsync(dbItem);
        }
    }
}