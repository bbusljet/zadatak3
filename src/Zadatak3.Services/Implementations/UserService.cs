﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Zadatak3.Models.Models;
using Zadatak3.Services.Interfaces;
using Zadatak3.Repository.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Zadatak3.Data.DbModels;
using AutoMapper;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Zadatak3.Services.Implementations
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            var dbUser = Mapper.Map<DbUser>(user);
            return await _userRepository.AddToRoleAsync(dbUser, role);
        }

        public async Task<IdentityResult> CreateRoleAsync(IdentityRole<Guid> role)
        {
            return await _userRepository.CreateRoleAsync(role);
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            var user = await _userRepository.FindByNameAsync(userName);

            return Mapper.Map<User>(user);
        }

        public async Task<List<User>> GetCurrentUserProfileAsync(User user)
        {
            var dbUser = Mapper.Map<User, DbUser>(user);

            var userProfile = await _userRepository.GetUserProfileAsync();

            var filterProfile = userProfile.FindAll(p => p.UserProfile != null && p.UserProfile.UserId == dbUser.Id);

            return Mapper.Map<List<User>>(filterProfile);
        }

        public async Task<User> GetUserIdAsync(ClaimsPrincipal principal)
        {
            var userId = await _userRepository.GetUserIdAsync(principal);

            return Mapper.Map<User>(userId);
        }

        public async Task<bool> IsinRoleAsync(User user, string role)
        {
            var dbUser = Mapper.Map<DbUser>(user);
            return await _userRepository.IsinRoleAsync(dbUser, role);
        }

        public bool isSignedIn(ClaimsPrincipal principal)
        {
            return _userRepository.isSignedIn(principal);
        }

        public async Task LogOffAsync()
        {
            await _userRepository.LogOffAsync();
        }

        public async Task<SignInResult> PasswordLoginAsync(string username, string passwordHash, bool isPersistent, bool lockout)
        {
            return await _userRepository.PasswordLoginAsync(username, passwordHash, isPersistent, lockout);
        }

        public async Task<IdentityResult> RegisterAsync(User user, string password)
        {
            var dbUser = Mapper.Map<DbUser>(user);
            return await _userRepository.RegisterAsync(dbUser, password);
        }
    }
}